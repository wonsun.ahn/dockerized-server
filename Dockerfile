# Build stage
FROM python:3.9.4-slim as builder

# install pre-requisites
RUN apt-get update && apt-get install -y libpq-dev gcc libffi-dev build-essential python-dev

# install dependencies
WORKDIR /app
COPY requirements.txt .
RUN pip install --user --no-warn-script-location -r requirements.txt

# copy over app files
COPY flaskr flaskr
COPY config.py .
COPY wait-for-postgres.sh .

# app stage
FROM python:3.9.4-slim as app

RUN apt-get update && apt-get install -y postgresql-client

# define environment variables
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP flaskr

# copy over built dependencies and application code from builder
COPY --from=builder /root/.local /root/.local
COPY --from=builder /app         /app

# define working directory
WORKDIR /app
# update path to find locally installed packages
ENV PATH=/root/.local/bin:$PATH

# define default command
CMD ["/bin/bash", "-c", "flask run --host 0.0.0.0"]
